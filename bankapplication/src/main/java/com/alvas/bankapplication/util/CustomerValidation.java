package com.alvas.bankapplication.util;

import org.springframework.stereotype.Component;

@Component
public class CustomerValidation {

	public boolean isEmailValid(String inputEmail) {
		String email = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
		if (inputEmail.matches(email)) {
			return true;
		} else
			return false;
	}

	public boolean isPhoneNumberValid(Long phoneNumber) {
		String phonePattern="^[0-9]+$";
		String phoneNum=Long.toBinaryString(phoneNumber);
		return  phoneNum.matches(phonePattern);
	}
	 
	public boolean isPanNumberValid(String panNumber)
	{
		String pan="[A-Z]{5}[0-9]{4}[A-Z]{1}";
		return  panNumber.matches(pan);
	}
	
}
