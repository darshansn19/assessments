package com.alvas.bankapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.bankapplication.dto.Customer;
import com.alvas.bankapplication.dto.ResponseDto;
import com.alvas.bankapplication.serviceimpl.CustomerServiceImple;
import com.alvas.bankapplication.util.CustomerValidation;

@RestController
public class CustomerController {
	@Autowired
	CustomerServiceImple customerServiceImple;

	@Autowired
	CustomerValidation customerValidation;

	@PostMapping(value = "/customers")
	public ResponseDto createCustomerAccount(@RequestBody Customer customer) {
		ResponseDto responseDto = new ResponseDto();
		if (customer.getFname().isEmpty() == true) {
			responseDto.setMessage("First name is required");
			return responseDto;
		}
		if (customer.getLname().isEmpty() == true) {
			responseDto.setMessage("Last name is required");
			return responseDto;
		}
		if (customer.getAge() < 18) {
			responseDto.setMessage("Age should be minimum 18");
			return responseDto;
		}
		if (customerValidation.isEmailValid(customer.getEmail()) == false) {
			responseDto.setMessage("invalid email id");
			return responseDto;
		}
		if (customer.getGender().isEmpty() == true) {
			responseDto.setMessage("Gender is required");
			return responseDto;
		}
		if (customerValidation.isPhoneNumberValid(customer.getPhone()) == false) {
			responseDto.setMessage("phone number is required");
			return responseDto;
		}
		if (customerValidation.isPanNumberValid(customer.getPanNumber()) == false) {
			responseDto.setMessage("pan number is required");
			return responseDto;
		}
		return customerServiceImple.createCustomerAccount(customer);

	}

}
