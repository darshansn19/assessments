package com.alvas.bankapplication.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.bankapplication.dto.Transaction;
import com.alvas.bankapplication.serviceimpl.AccountServiceImple;

@RestController
public class TransactionController {

	@Autowired
	AccountServiceImple accountServiceImple;

	@GetMapping(value = "customers/transaction")
	public List<Transaction> getTransactionDetails(@RequestParam long fromAccountNum, String transactionDate) {
		LocalDate fromDate = LocalDate.parse(transactionDate + "-01",
				DateTimeFormatter.ofPattern("yyyy-MMM-d", Locale.ENGLISH));
		LocalDate toDate = LocalDate.parse(transactionDate + "-31",
				DateTimeFormatter.ofPattern("yyyy-MMM-d", Locale.ENGLISH));
		return accountServiceImple.getStatements(fromAccountNum, fromDate, toDate);

	}
}
