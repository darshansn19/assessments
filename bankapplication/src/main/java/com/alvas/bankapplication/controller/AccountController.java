package com.alvas.bankapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.bankapplication.dto.ResponseDto;
import com.alvas.bankapplication.dto.Transaction;
import com.alvas.bankapplication.serviceimpl.AccountServiceImple;

@RestController
public class AccountController {

	@Autowired
	AccountServiceImple accountServiceImple;

	@PostMapping(value = "/Customers/account")
	public ResponseDto fundTransfer(@RequestBody Transaction transaction) {

		if (transaction.getAmount() > 0) {
			return accountServiceImple.fundTransfer(transaction.getFromAcconutNum(), transaction.getToAccountNum(),
					transaction.getAmount());
		} else {
			ResponseDto responseDto = new ResponseDto();
			responseDto.setMessage("insufficient balance");
			return responseDto;
		}

	}

}
