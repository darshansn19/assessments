package com.alvas.bankapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvas.bankapplication.dto.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

	Account findByAccNum(long accNum);

}
