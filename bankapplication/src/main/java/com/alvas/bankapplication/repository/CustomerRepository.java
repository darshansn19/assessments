package com.alvas.bankapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvas.bankapplication.dto.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
