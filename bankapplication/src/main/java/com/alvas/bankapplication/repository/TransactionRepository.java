package com.alvas.bankapplication.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvas.bankapplication.dto.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	List<Transaction> findByFromAcconutNumAndTransactionDateBetween(long fromAcconutNum, LocalDate fromDate,
			LocalDate todate);

}
