package com.alvas.bankapplication.service;

import java.time.LocalDate;
import java.util.List;

import com.alvas.bankapplication.dto.ResponseDto;
import com.alvas.bankapplication.dto.Transaction;

public interface AccountService {

	ResponseDto fundTransfer(long fromAcconutNum, long toAccountNum, double amount);

	List<Transaction> getStatements(long fromAccountNum, LocalDate fromDate, LocalDate toDate);

}
