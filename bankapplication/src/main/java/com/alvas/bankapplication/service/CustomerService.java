package com.alvas.bankapplication.service;

import com.alvas.bankapplication.dto.Customer;
import com.alvas.bankapplication.dto.ResponseDto;

public interface CustomerService {

	ResponseDto createCustomerAccount(Customer customer);

}
