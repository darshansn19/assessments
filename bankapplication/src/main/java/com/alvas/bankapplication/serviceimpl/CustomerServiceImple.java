package com.alvas.bankapplication.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alvas.bankapplication.dto.Customer;
import com.alvas.bankapplication.dto.ResponseDto;
import com.alvas.bankapplication.repository.AccountRepository;
import com.alvas.bankapplication.repository.CustomerRepository;
import com.alvas.bankapplication.service.CustomerService;

@Service
public class CustomerServiceImple implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Override
	public ResponseDto createCustomerAccount(Customer customer) {

		if (customer.getAccount().getAccType().equals("saving")
				|| (customer.getAccount().getAccType().equals("current"))) {
			customer.getAccount().setAccNum((long) (Math.random() * Math.pow(10, 10)));
			customer.getAccount().setAccType(customer.getAccount().getAccType());
			customer.getAccount().setBalance(customer.getAccount().getBalance());
			customerRepository.save(customer);
			ResponseDto responseDto = new ResponseDto();
			responseDto.setMessage("Account created successfully");
			return responseDto;
		} else {
			ResponseDto dto = new ResponseDto();
			dto.setMessage("select account type either saving or current");
			return dto;
		}

	}
}
