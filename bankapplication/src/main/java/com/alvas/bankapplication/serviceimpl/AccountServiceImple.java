package com.alvas.bankapplication.serviceimpl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alvas.bankapplication.dto.Account;
import com.alvas.bankapplication.dto.ResponseDto;
import com.alvas.bankapplication.dto.Transaction;
import com.alvas.bankapplication.repository.AccountRepository;
import com.alvas.bankapplication.repository.TransactionRepository;
import com.alvas.bankapplication.service.AccountService;

@Service
public class AccountServiceImple implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@Override
	@Transactional
	public ResponseDto fundTransfer(long fromAccountNum, long toAccountNum, double amount) {
		ResponseDto responseDto = new ResponseDto();
		Account fromAccountNumber = accountRepository.findByAccNum(fromAccountNum);
		Account toAccountNumber = accountRepository.findByAccNum(toAccountNum);
		if ((fromAccountNumber != null && toAccountNumber != null) && (fromAccountNumber != toAccountNumber)) {
			if (amount <= fromAccountNumber.getBalance()) {
				fromAccountNumber.setBalance(fromAccountNumber.getBalance() - amount);
				toAccountNumber.setBalance(toAccountNumber.getBalance() + amount);

				Transaction fundTransfer = new Transaction();
				fundTransfer.setFromAcconutNum(fromAccountNum);
				fundTransfer.setToAccountNum(toAccountNum);
				fundTransfer.setAmount(amount);
				fundTransfer.setStatus("debited from the account:" + fundTransfer.getFromAcconutNum());
				transactionRepository.save(fundTransfer);

				Transaction fundTransfer2 = new Transaction();
				fundTransfer2.setFromAcconutNum(toAccountNum);
				fundTransfer2.setToAccountNum(fromAccountNum);
				fundTransfer2.setAmount(amount);
				fundTransfer2.setStatus("credited to the account :" + fundTransfer.getToAccountNum());
				transactionRepository.save(fundTransfer2);

				responseDto.setMessage("transaction is successfull");
				return responseDto;

			} else {
				responseDto.setMessage("insufficient balance");
				return responseDto;
			}
		} else {
			responseDto.setMessage("account  not found");
			return responseDto;
		}
	}

	@Override
	public List<Transaction> getStatements(long fromAccountNum, LocalDate fromDate, LocalDate todate) {
		return transactionRepository.findByFromAcconutNumAndTransactionDateBetween(fromAccountNum, fromDate, todate);

	}
}
