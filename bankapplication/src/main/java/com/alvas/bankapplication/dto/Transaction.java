package com.alvas.bankapplication.dto;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int transactionId;

	@NotNull
	private long fromAcconutNum;

	@NotNull
	private long toAccountNum;

	@NotNull
	private double amount;

	@CreationTimestamp
	private LocalDate transactionDate;

	private String Status;

}
