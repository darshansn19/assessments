package com.alvas.bankapplication.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;

	@NotNull
	private String fname;

	@NotNull
	private String lname;

	@NotNull
	@Min(value = 18, message = "age should be minimum 18")
	private int age;

	@NotNull(message = "gender is required")
	private String gender;

	@NotNull
	@Email(message = "email is required with correct form")
	@Column(unique = true)
	private String email;

	@NotNull(message = "phone is mandatory")
	@Column(unique = true)
	private long phone;

	@NotNull(message = "PanNumber is required")
	private String panNumber;

	@OneToOne
	@Cascade(CascadeType.ALL)
	private Account account;

}
